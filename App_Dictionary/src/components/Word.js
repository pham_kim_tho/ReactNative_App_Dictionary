import React, { Component } from 'react';
import {View, Text,StyleSheet,TouchableOpacity, Alert,TextInput,Dimensions} from 'react-native';
import Button from './controls/Button';



export default class Word  extends Component {
    constructor(props){
        super(props);
        this.state = {
            username: ''
        }
    }
    render() {
        const {en,vn} = this.props;
        return (
            <View>
                <Text style={styles.en}>{en}</Text>
                <Text style={styles.vn}>{vn}</Text>
                <TextInput 
                    style={styles.input}
                    placeholder='Tên đăng nhập'
                    // value={this.state.username}
                    onChangeText={text => this.setState({username: text})}
                ></TextInput>
                <Button 
                    text='Hide' 
                    onPress={() => Alert.alert('Thông báo',this.state.username)}
                    // onPress={() => console.log(this.state.username)}
                    style={{backgroundColor: '#1F6F9E'}}></Button>
            </View>
        );
    }
}

const styles = new StyleSheet.create({
    en: {
        color: 'green',
        fontSize: 30
    },
    vn: {
        fontSize: 20,
        color: 'blue'
    },
    input: {
        backgroundColor: '#FFE680',
        width: 300,
        height: 40,
        paddingHorizontal: 5,
        margin: 5
    }
})