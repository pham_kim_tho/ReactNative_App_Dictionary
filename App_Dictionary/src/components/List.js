import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, FlatList, Image,Dimensions } from 'react-native';
import Word from './Word';

const defaultWords = [
    { en: 'hello', vn: 'xin chào', isMemorized: false, id: '1' },
    { en: 'good morning', vn: 'chào buổi sáng', isMemorized: false, id: '2' },
    { en: 'software', vn: 'phần mềm', isMemorized: false, id: '3' },
    { en: 'system', vn: 'hệ thống', isMemorized: false, id: '4' },
    { en: 'teach', vn: 'công nghệ, kỹ thuật', isMemorized: false, id: '5' },
    { en: 'sun', vn: 'mặt trời', isMemorized: false, id: '6' }
];

export default class List extends Component {
    render() {
        return (
            <View>
                {/* <Image
                    source={require('/react-native/img/favicon.png')}
                /> */}
                <Image
                    style={{ width: 50, height: 50 }}
                    source={{ uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png' }}
                />
                <FlatList
                    data={defaultWords}
                    renderItem={({ item }) => <Word {...item}></Word>}
                    keyExtractor={item => item.id}
                />
                {/* {defaultWords.map(word => (
                    <View key={word.en}>
                        <Text>{word.en}</Text>
                        <Text>{word.vn}</Text>
                    </View>
                ))} */}
            </View>
        );
    }
}

const styles = new StyleSheet.create({

})

